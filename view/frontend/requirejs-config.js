
var config = {

	map: {
        '*': {
            "smcountdowntimer": "Kowal_SiteMaintenance/js/countdowntimer",
            "jquery.countdown.sm": "Kowal_SiteMaintenance/js/jquery.countdown"
        }
    },
    "shim" : {
        "jquery.countdown.sm": ["jquery"]
    }};